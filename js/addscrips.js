function include(url) {
    var script = document.createElement('script');
    script.src = PATH + url;
    document.getElementsByTagName('head')[0].appendChild(script);
}
PATH = 'js/model/';
include('Users.js');
include('Product.js');
include('Cart.js');
include('Shop.js');
include('interface.js');

PATH = 'js/view/';
include('draw.js');
include('Page.js');

PATH = 'js/view/pages/';
include('loginPage.js');
include('shopPage.js');
include('checkPage.js');

PATH = 'js/';
include('controller.js');

// function include(url){
//     $('head')[0].append(
//         $('<script>', {
//             src: 'js/module/'+url
//         })
//     );
// }