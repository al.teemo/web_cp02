// ========== creation page of login ==========
function LoginPage(data){
    Page.call(this, data);
    this.myname = 'login';
    var that = this;
    
    this.render = function(){
        console.log('login render')
        Drow.clearBody();
        $('<div>', {
            id: 'loginPaper',
            class: 'fullScreen',
            append: $('<form>', {
                name: 'loginForm'
            }).append(
                $('<div>', {
                    id: 'welcome'
                    // text: ''
                }).append($('<h1>', {text: 'Login, pls'}),[
                    $('<br>'),
                    Drow.crInput('login', 'text'), 
                    Drow.crInput('password', 'text'), 
                    Drow.crInput('logok', 'button').val('Login')
                ])
            )
        }).appendTo('body');

    // navigation
        $('#logok').on('click', function(event){            
            Controller.checkLogin( 
                Page.nextPage,  // call if success
                that.sayWrong,  // call if failure
                $('#login').val(), 
                $('#password').val() 
            );
        })
    }
    this.sayWrong = function() {
        console.log('wrong pass or log');
        $('#welcome').append(
            $('<h3>', {
                id: 'wrong',
                text: 'Wrong login or password'
            })
        );
        setTimeout(() => {
            console.log('deleted wrong text');
            $('#welcome h3:first').remove();
        }, 2000);
    }
}
// LoginPage.prototype = Object.create(Page.prototype);
// LoginPage.prototype.constructor = LoginPage;
// LoginPage.prototype = Page;


// Page.newPage(new Page('Page'));
// Page.newPage(new LoginPage('Login'));
// Page.getStart();
// Page.prt();
// Page.prt();
// Page.prt();

    