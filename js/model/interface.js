// === login ===
function tryLogin(login, password) {
    return {
        'code' : shop.tryLogin(login, password)
    }
}
function getUsername(){
    var res = {
        "data" : shop.getUsername()
    }
    res.code = (res.data ? 0 : 1);
    return res;
}
// --- tables ---
function getTableHeads(){
    return {
        "prod": shop.getProdHead(),
        "cart": shop.getCartHead()
    }
}
// === product table ===
function getProdTable() {
    return {
        "code" : 0,
        "data" : shop.getTable()
    };
}
// this.getProdCount = function(id){
//     if(id) return table.getCount(id);
//     else return '---';
// }

// === cart table ===
//?????????????????????????????????????????????????????????
// ??? how to separate and optimise craft table + total ???
//?????????????????????????????????????????????????????????
// function getCartTotal(){
//     var total = 0;
//     console.log(cart.getView());
//     cart.getView().forEach((row) => {// repeats
//         if(row != null){
//             total += table.getPrice(row) * cart.getCount(row);
//             console.log('row: '+row+' count: '+cart.getCount(row)+' ')
//         }
//     });
//     return total;
// }
function getCartTable() {
    return {
        "code" : 0,
        "data" : shop.getCartTable()
    }
}
function addToCart(id, cnt) {
    // cnt = Number(cnt);
    var result = {
        "code" : shop.addToCart(id, cnt)
    }
    if(!result.code){
        result.data = getCartTable().data;
    }
    // console.log('interface')
    // console.log(result)
    return result;
}
function removeFromCart(id) {
    return {
        "code" : shop.removeFromCart(id)
    }
}
function decrementInCart(id) {
    console.log('dec')
    return {
        "code" : shop.decrementInCart(id, 1)
    }
}

// === check table ===
function getCheck() {
    var report = getCartTable();
    if(!report.code)
        report.data.head = shop.getCheckHead();
    return report;
}

shop = new Shop();
// table = new ProductTable();
// cart = new CartTable();
// user = new Users();
