console.log('product loaded');


function ProductTable(){
    var table = [];

    this.remove = function (id, count){
        if(table[id].count < +count)
            return 1;
        table[id].count -= +count;
        return 0;
    }
    this.add = function (id, count){
        if(count){
            table[id].count += +count;
            return 0;
        }
        return 1;
    }

    this.parseList = function(){
        var list = [
            ['Car', '999999', 1],
            ['Baby', '100.5', 100],
            ['Gun', '10000', 15]
        ];

        list.forEach((prod, id) => {
            table.push({
                "name" : prod[0],
                "price" : prod[1],
                "count" : prod[2]
            })
        })
    }

    this.getTable = function (){
        var list = [];
        table.forEach((prod, id) => {
            list.push(this.getById(id));
        });
        return list;
    }
    this.getById = function(id){
        var prod = table[id];
        return [
            prod.name,
            prod.price,
            prod.count
        ]
    }
    this.getAttr = function(id, attr){
        return table[id][attr];
    }

    this.parseList();
}
